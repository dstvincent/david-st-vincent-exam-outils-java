package exam;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.logging.Level;

import org.junit.Test;

/** Test pour JUnit.
 */
public class CompteNombreTest {
	/** constante pour les tests.
	 */
	static final int CONSTANTE_TEST = 9;
	

	/** testera 5 valeurs différentes.
	 */
	@Test
	public final void test() {
		try {
			final int n1 = CompteNombre.compterNombreNonRepetes(2);
			assertTrue(n1 == 1);
			
			final int n2 = CompteNombre.compterNombreNonRepetes(10);
			assertTrue(n2 == 2);
			
			final int n3 = CompteNombre.compterNombreNonRepetes(123456789);
			assertTrue(n3 == CONSTANTE_TEST);
			
			final int n4 = CompteNombre.compterNombreNonRepetes(100000);
			assertTrue(n4 == 2);
			
		} catch (Exception e) {
			fail("test echoue, erreur inattendue.");
			ClLogger.getInstance().ajouterLog(Level.SEVERE, 
					"ERREURE MAJEURE DURANT TEST", e);
		}
	}

}
