package exam;

import java.util.logging.Level;
import java.util.logging.Logger;

/** Loggeur, �quivalent a Log4J.
 * 
 * @author David ST-V
 *
 */
public final class ClLogger {
	/** cette classe est singleton.
	 */
    private static ClLogger instance = new ClLogger();
    /** cette donn�e membre est le logger.
     */
    private Logger logger;

    /** Constructeur.
     */
    private ClLogger() {
    	logger = Logger.getLogger(ClLogger.class.getName());
    }

    /** Instance du singleton.
     * @return l'instance unique
     */
    public static ClLogger getInstance() {
        return instance;
    }

    /** fonction qui ajoute le log dans le texte.
     * @param level la gravit� de l'erreur
     * @param texte le texte de l'erreur
     * @param e l'exception lev�e
     */
    public void ajouterLog(final Level level, final String texte,
    		final Exception e)
    {
    	logger.log(level, texte, e);
    }
}


