package exam;

/** classe de l'examen final d'outils.
 * @author David St-Vincent
 */
public final class CompteNombre {
	/** Constante pour �viter une erreur checkstyle.
	 */
	static final int CONSTANTE_NOMBRE = 10;
	/** Constructeur priv� pour ne pas que checkstyle pleure.
	 * 
	 */
	private CompteNombre() { }
	
	/** fonction qui calcule le nombre de chiffre diff�rents
	 *  d'un nombre pass� en parametre.
	 * @param n le nombre
	 * @return le nombre de chiffre pass� par param�tre
	 */
	public static int compterNombreNonRepetes(final int n) {
		try {
			 int nouveauN = n;
			 int save = n;
			 int nb = 0;
			 while (nouveauN > 0) {
			 nb++;
			 nouveauN /= CONSTANTE_NOMBRE;
			 }
			 if (nb == 0) {
			 return 0;
			 }
			 int s = 0;
			 for (int i = 0; i < nb; i++) {
			 nouveauN = save;
			 int c = nouveauN % CONSTANTE_NOMBRE;
			 nouveauN /= CONSTANTE_NOMBRE;
			 while (nouveauN > 0) {
			 if (nouveauN % CONSTANTE_NOMBRE == c) {
			 break;
			 }
			 nouveauN /= CONSTANTE_NOMBRE;
			  }
			  if (nouveauN == 0) {
			  s++;
			  }
			  save /= CONSTANTE_NOMBRE;
			  }
			 return s; 
			 } catch (Exception e) {
			System.out.println("ERREUR CRITIQUE");
			return 0;
		}
	}
	/** Module principal.
	 * @param args les arguments du programme
	 */
	public static void main(final String[] args) {
		int nombre = CONSTANTE_NOMBRE;
		System.out.println("Nombres non r�p�t�s :"
					+ compterNombreNonRepetes(nombre));
	}
}

